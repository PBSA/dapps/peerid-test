import React, { useState } from 'react';
import logo from './peerplays.png';
import * as peerplaysuri from 'peerplays-uri'
import './App.css';

function App() {
  const [memo, setMemo] = useState("");
  
  const handleSubmit = (evt) => {
    evt.preventDefault();
    const url = peerplaysuri.encodeOp(['transfer', {from: '__signer', to: 'nathan', amount: '0.00001 PPY', memo: memo}])
      .replace('peerplays://','https://signer.peerplays.download/');
    window.open(url);
  }

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <br/>
        <a
          className="App-link"
          href="https://signer.peerplays.download/login"
          target="_blank"
          rel="noopener noreferrer"
        >
          Test Peerplays Login
        </a>
        <br/>
        <form onSubmit={handleSubmit}>
          <label>
            Message:
            <input
              type="text"
              value={memo}
              onChange={e => setMemo(e.target.value)}
            />
          </label>
          <input type="submit" value="Send Test Memo" />
        </form>
      </header>
    </div>
  );
}

export default App;
